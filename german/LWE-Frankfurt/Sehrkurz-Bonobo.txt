Das GNOME-Komponentensystem Bonobo stellt f�r GNOME das Grundger�st zur
Erstellung und Implementierung wiederverwendbarer Softwarekomponenten dar.
Diese Komponenten stellen ihre Funktionalit�t �ber wohldefinierte
Schnittstellen zur Verf�gung und sind zudem voneinander unabh�ngig sowie
zur Laufzeit austauschbar.
